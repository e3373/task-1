/*Task 5:
Display a list of products from the "products.json" file as a table. The data in this table should have
the ability to be filtered and sorted.
Displaying:
The "Options" button should be displayed. Below "Options" button the list of all products should be
displayed as a table, which will contain the following columns: "#" (row number in the table),
"Category", "Price", "Manufacturer" and "Production date". Above the table of products should also be
displayed: total quantity, total cost, average price of displayed products, the most expensive product
and it's price, as well as the cheapest product and it's price.
Filtering:
By clicking on the "Options" button, the side panel should open. Side panel should contain various
filtering settings and the control buttons such as "Apply" and "Reset". By clicking on the "Apply"
button, the filtering and sorting settings should be applied, the side panel should be closed and the
data in the table should change accordingly to the applied settings. By clicking on the "Reset" button,
all the previously selected settings should be reset and the panel should be closed.
● Filter by category:
A list of checkboxes with unique categories should be displayed in the side panel. This list 
could be extracted from the list of all products. There can be multiple selected categories.
Additionally, there should be an "All except" option. If it is active, the list of products should
be filtered by all categories except those that were selected.
● Filter by price:
Two input fields should be displayed in the side panel. One of them is responsible for the
lowest price, the other one is the highest price.
● Filter by manufacturer:
Same functionality as for category.
Sorting:
By clicking on the heading cell in the table, sorting should be performed. Sorting can be ascending
and descending.*/

import {Link, NavLink} from 'react-router-dom';
import {Navbar, Nav} from 'react-bootstrap';
import {useRef, useState, useEffect} from 'react';
import {Table, Col, Form, Button} from "react-bootstrap";
import productData from "./../data/productData";
import ProductTable from './../components/ProductTable'

	export default function Task5(){

		const [priceMin, setPriceMin] = useState(0)
		const [priceMax, setPriceMax] = useState(Math.pow(10, 1000)) //infinity
		const [products, setProducts] = useState()
		const [shoes, setShoes] = useState(false)
		const [clothes, setClothes] = useState(false)
		const [perfume, setPerfume] = useState(false)

		//for table note: this method is not ideal

		const productsData = productData.map( product => {	
			if (shoes == false && clothes && perfume){
				if(product.price >= priceMin && product.price <= priceMax && product.category == "Shoes"){
					return (
						<ProductTable key= {product.id} productProp={product}/>
					)
				}
			} else if (shoes == false && clothes == false && perfume){
				if(product.price >= priceMin && product.price <= priceMax && (product.category == "Shoes" || product.category == "Clothes")){
					return (
						<ProductTable key= {product.id} productProp={product}/>
					)
				}
			} else if (shoes == false && clothes == false && perfume == false){
				if(product.price >= priceMin && product.price <= priceMax && (product.category == "Shoes" || product.category == "Clothes" || product.category == "Perfume")){
					return (
						<ProductTable key= {product.id} productProp={product}/>
					)
				}
			} else if (shoes && clothes && perfume){
				if(product.price >= priceMin && product.price <= priceMax && product.category == "Shoes" && product.category == "Clothes" && product.category == "Perfume"){
					return (
						<ProductTable key= {product.id} productProp={product}/>
					)
				}
			}
		})


		function updateTable(){
			const productsData = productData.map( product => {	
				
					return (
						<ProductTable key= {product.id} productProp={product}/>
					)
				
			})
		}


		//for side panel
		const primaryNav = useRef();
		const boxRef = useRef();

		//toggle side panel
		function togglePanel(){

		  const visibility = primaryNav.current.getAttribute("data-visible")

		  if (visibility === "false"){
		    primaryNav.current.setAttribute("data-visible", true)
		  } else {
		    primaryNav.current.setAttribute("data-visible", false)
		  }

		}
		console.log(shoes)
		console.log(clothes)
		console.log(perfume)

	return (
		<>	
			<header>
			  <nav ref={boxRef}>
			    <ul ref={primaryNav} id="primary-navigation" className="primary-navigation d-flex" data-visible="false">
			      
				    <Form>
					    <li>
					      	<h2>CATEGORY</h2>
					    </li>
					    <li>
					      	<label className="mx-3"><input type="checkbox" onChange={(e) => setShoes(e.target.checked)}/> Shoes</label>
					    </li>
					    <li>
					      	<label className="mx-3"><input type="checkbox" onChange={(e) => setClothes(e.target.checked)}/> Clothes</label>
					    </li>
					    <li>
					      	<label className="mx-3 mb-3"><input type="checkbox" onChange={(e) => setPerfume(e.target.checked)}/> Perfume</label>
					    </li>
					    <li>
					    	<button className="btn btn-success mb-5">All EXCEPT</button>
					    </li>



				        <li>
				        	<h2>MANUFACTURER</h2>
				        </li>
				        <li>
				        	<label className="mx-3"><input type="checkbox" /> Nike</label>
				        </li>
				        <li>
				        	<label className="mx-3"><input type="checkbox"/> Under Armour</label>
				        </li>
				        <li>
				        	<label className="mx-3 mb-3"><input type="checkbox"/> Adidas</label>
				        </li>
				        <li>
				        	<button className="btn btn-success mb-5">All EXCEPT</button>
				        </li>



					    <li>
					      	<h2>PRICE</h2>
					    </li>
					    <li>
					      	<label className="btn btn-secondary" onChange={(e) => setPriceMin(e.target.value)}>Lowest Price: <input type="number"/></label>
					    </li>
					    <li>
					      	<label className="btn btn-secondary" onChange={(e) => setPriceMax(e.target.value)}>Highest Price: <input type="number"/></label>
					    </li>



			            <li>
			    	      	<button type="submit" onClick={updateTable} className="btn btn-primary my-5 mx-5">APPLY</button>
			    	      	<button className="btn btn-warning">RESET</button>
			            </li>
			        </Form>
				</ul>


			  </nav>
			</header>

			<Table striped bordered hover variant="dark">
			  <thead>
			    <tr>
			      <th>total quantity</th>
			      <th>total cost</th>
			      <th>average price of displayed</th>
			      <th>most expensive product:</th>
			      <th>productionDate</th>
			    </tr>
			  </thead>
			</Table>
			<Table striped bordered hover>
			  <thead>
			    <tr>
			      <th>#</th>
			      <th>Category</th>
			      <th>price</th>
			      <th>Manufacturer</th>
			      <th>productionDate</th>
			    </tr>
			  </thead>
			  <tbody>
			  	{productsData}
			  </tbody>
			</Table>
			<button onClick={togglePanel} className="btn btn-success m-5">OPTIONS</button>
		</>
	)
}