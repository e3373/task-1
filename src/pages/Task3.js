/*Task 3:
Create a short program that will simulate a fight between two fighters. Each fighter must be created
using the Fighter constructor function. When creating a new fighter using this function, you need to
pass to it (all properties, except for the name, must contain a number of points; max number of
points that can be distributed is 30):
● name
● strength
● agility
● vitality
Each of these parameters will affect the hp, damage and defense of the fighter. These parameters
should not be object properties, can't be directly modified in the object by assigning new values and
should not even be readable. Instead, each fighter should have these methods:
● getName - returns the name of the fighter
● getHp - returns hp of the fighter
● takeDamage - deals damage to the fighter who called this method (the amount of damage
should be passed as an argument to this method)
● dealDamage - deals damage to the rival (rival should be passed as an argument to this
method)
The base damage of a fighter is 10. Each point of strength increases damage by 5, agility decreases
damage by 3.
The base defense of a fighter is 10. Each point of agility increases defence by 5, strength increases
defence by 3, vitality increases defence by 1.
The base hp of a fighter is 50. Each point of vitality increases hp by 10, strength increases hp by 5,
agility increases hp by 3.
Create a fight function that takes two fighters as arguments. In this function, they must take turns
inflicting damage to each other until the hp level of one of them reaches 0 (or lower). After each
attack, a message should be displayed, which will indicate the amount of hp and the name of the
fighter who received damage. When one of the fighters has been defeated, a message should
appear indicating the name of the winner.*/

import {useState} from 'react';

export default function Task3(){

/*	const [pika, setPika] = useState([]);
	const [char, setChar] = useState([]);*/

	function Fighter(name, strength, agility, vitality) {
		this.name = name;
		this.strength = strength;
		this.agility = agility;
		this.vitality = vitality;
		this.attack = 10 + (strength * 5) - (agility * 3);
		this.hp = 50 + (vitality * 10) + (strength * 5) + (agility * 3);

		this.getName = function(){
			/*console.log(name)*/
		}
		this.getHp = function(){
			/*console.log(hp)*/
		}
		this.damage = function(target){
				console.log(`${this.name} damaged ${target.name} by ${this.attack}`);
				console.log(`${target.name}'s health is now reduced from ${target.hp} to ${target.hp - this.attack}`)
				if (target.hp - this.attack <=0) {
					console.log(`${target.name} fainted`)
					}
				let newTarget ={
					Name : target.name,
					Health : target.hp - this.attack,
					Attack : target.attack,
					Damage : target.damage
				}	
		}

	};
	let pikachu = new Fighter("Pikachu", 10,10,10);
	let charmander = new Fighter("Charmander", 16,9,5);	
	function fight() {
		//do conditional statements here
		pikachu.damage(charmander)
		charmander.damage(pikachu)
	}
	fight()

	return (
		<>
			<h1>FIGHTERS</h1>
			<p>see console</p>
		</>
	)
}