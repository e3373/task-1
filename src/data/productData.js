
const productData = 
[
    {
        "id": "1",
        "category": "Shoes",
        "price": 3000,
        "manufacturer": "Nike",
        "productionDate": "11-02-2021"

    },
    {
        "id": "2",
        "category": "Shoes",
        "price": 4000,
        "manufacturer": "Under Armour",
        "productionDate": "01-02-2020"

    },
    {
        "id": "3",
        "category": "Shoes",
        "price": 1000,
        "manufacturer": "Adidas",
        "productionDate": "12-02-2009"

    },
    {
        "id": "4",
        "category": "Clothes",
        "price": 500,
        "manufacturer": "Nike",
        "productionDate": "02-02-2022"

    },
    {
        "id": "5",
        "category": "Clothes",
        "price": 1500,
        "manufacturer": "Under Armour",
        "productionDate": "08-02-2021"

    },
    {
        "id": "6",
        "category": "Clothes",
        "price": 19000,
        "manufacturer": "Adidas",
        "productionDate": "02-01-2022"

    },
    {
        "id": "7",
        "category": "Perfume",
        "price": 5000,
        "manufacturer": "Adidas",
        "productionDate": "11-02-2021"

    },
    {
        "id": "8",
        "category": "Perfume",
        "price": 6000,
        "manufacturer": "Nike",
        "productionDate": "11-02-2021"

    }
]
export default productData;